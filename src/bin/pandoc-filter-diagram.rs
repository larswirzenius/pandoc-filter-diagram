//! A program that can be used with the pandoc --filter option.

use pandoc_filter_diagram::{pandoc_ast, DiagramFilter};
use std::io::{Read, Write};

fn main() {
    if let Err(err) = real_main() {
        eprintln!("ERROR: {}", err);
        std::process::exit(1);
    }
}

fn real_main() -> anyhow::Result<()> {
    let fail_on_error = if let Ok(v) = std::env::var("PANDOC_FILTER_FAIL") {
        v == "1"
    } else {
        false
    };

    let mut df = DiagramFilter::new();
    let mut json = String::new();
    std::io::stdin().read_to_string(&mut json)?;
    let json = pandoc_ast::filter(json, |doc| df.filter(doc));
    if !df.errors().is_empty() {
        for e in df.errors().iter() {
            eprintln!("ERROR: {}", e);
        }
        if fail_on_error {
            eprintln!("Failing as requested");
            std::process::exit(1);
        }
    }
    std::io::stdout().write_all(json.as_bytes())?;
    Ok(())
}
