# Introduction

This document specifies the acceptance criteria for
`pandoc-filter-diagram`, and how they are verified. It's meant to be
processed with the [Subplot](https://subplot.liw.fi/) tool. The
`build.rs` script makes sure the verification scenarios defined here
are run when `cargo test` is run.

The code in `pandoc-filter-diagram` was originally part of Subplot. It
was extracted into its own crate for use in the [Sequoia][] sq user guide.

[Sequoia]: https://sequoia-pgp.org/


# Pikchr

[Pikchr]: https://pikchr.org/

[Pikchr] is a diagramming library which implements a Pic-like diagram language.
It allows the conversion of textual descriptions of arbitrarily complex diagrams
into SVGs such as this one.

~~~pikchr
arrow right 200% "Markdown" "Source"
box rad 10px "Subplot" "Document Generator" "(subplot docgen)" fit
arrow right 200% "HTML+SVG/PDF" "Output"
arrow <-> down 70% from last box.s
box same "Pikchr" "Formatter" "(docs.rs/pikchr)" fit
~~~

This scenario checks that an image is generated and embedded into the HTML output,
and is not referenced as an external image.

~~~scenario
given an installed Rust program pandoc-filter-diagram
given file pikchr.md
when I run pandoc --filter pandoc-filter-diagram pikchr.md -o pikchr.html
then file pikchr.html matches regex /img src="data:image/svg\+xml;base64,/
~~~

~~~~~~~~{#pikchr.md .file .markdown .numberLines}
This is an example markdown file that embeds a simple Pikchr diagram.

~~~pikchr
arrow right 200% "Markdown" "Source"
box rad 10px "Markdown" "Formatter" "(docs.rs/markdown)" fit
arrow right 200% "HTML+SVG" "Output"
arrow <-> down 70% from last box.s
box same "Pikchr" "Formatter" "(docs.rs/pikchr)" fit
~~~
~~~~~~~~



# Graphviz Dot

[Graphviz]: http://www.graphviz.org/

Dot is a program from the [Graphviz][] suite to generate directed
graphs, such as this one.

~~~dot
digraph "example" {
thing -> other
}
~~~

The scenario checks that an image is generated and embedded into the
HTML output, not referenced as an external image.

~~~scenario
given an installed Rust program pandoc-filter-diagram
given file dot.md
when I run pandoc --filter pandoc-filter-diagram dot.md -o dot.html
then file dot.html matches regex /img src="data:image/svg\+xml;base64,/
~~~

~~~~~~~~{#dot.md .file .markdown .numberLines}
This is an example Markdown file, which embeds a graph using dot markup.

~~~dot
digraph "example" {
thing -> other
}
~~~
~~~~~~~~



# PlantUML

[PlantUML]: https://plantuml.com/

[PlantUML][] is a program to generate various kinds of UML diagrams
for describing software, such as this one:

~~~plantuml
@startuml
Alice -> Bob: Authentication Request
Bob --> Alice: Authentication Response

Alice -> Bob: Another authentication Request
Alice <-- Bob: Another authentication Response
@enduml
~~~

The scenario below checks that an image is generated and embedded into
the HTML output, not referenced as an external image.

~~~scenario
given an installed Rust program pandoc-filter-diagram
given file plantuml.md
when I run pandoc --filter pandoc-filter-diagram plantuml.md -o plantuml.html
then file plantuml.html matches regex /img src="data:image/svg\+xml;base64,/
~~~

~~~~~~~~{#plantuml.md .file .markdown .numberLines}
This is an example Markdown file, which embeds a diagram using
PlantUML markup.

~~~plantuml
@startuml
Alice -> Bob: Authentication Request
Bob --> Alice: Authentication Response

Alice -> Bob: Another authentication Request
Alice <-- Bob: Another authentication Response
@enduml
~~~
~~~~~~~~



# Roadmap

[roadmap]: https://crates.io/crates/roadmap

The [roadmap][] Rust library supports visual roadmaps using a YAML
based markup language.

An example:

~~~roadmap
goal:
  label: |
    This is the end goal:
    if we reach here, there
    is nothing more to be
    done in the project
  depends:
  - finished
  - blocked

finished:
  status: finished
  label: |
    This task is finished;
    the arrow indicates what
    follows this task (unless
    it's blocked)

ready:
  status: ready
  label: |
    This task is ready
    to be done: it is not
    blocked by anything

next:
  status: next
  label: |
    This task is chosen
    to be done next

blocked:
  status: blocked
  label: |
    This task is blocked
    and can't be done until
    something happens
  depends:
  - ready
  - next
~~~

This scenario checks that an image is generated and embedded into the
HTML output, not referenced as an external image.

~~~scenario
given an installed Rust program pandoc-filter-diagram
given file roadmap.md
when I run pandoc --filter pandoc-filter-diagram roadmap.md -o roadmap.html
then file roadmap.html matches regex /img src="data:image/svg\+xml;base64,/
~~~

~~~~~~~~{#roadmap.md .file .markdown .numberLines}
This is an example Markdown file, which embeds a roadmap.

~~~roadmap
goal:
  label: |
    This is the end goal:
    if we reach here, there
    is nothing more to be
    done in the project
  depends:
  - finished
  - blocked

finished:
  status: finished
  label: |
    This task is finished;
    the arrow indicates what
    follows this task (unless
    it's blocked)

ready:
  status: ready
  label: |
    This task is ready
    to be done: it is not
    blocked by anything

next:
  status: next
  label: |
    This task is chosen
    to be done next

blocked:
  status: blocked
  label: |
    This task is blocked
    and can't be done until
    something happens
  depends:
  - ready
  - next
~~~
~~~~~~~~


# Command line tool

The crate provides a command line tool `pandoc-filter-diagram`, for
use with the `pandoc --filter` option. By default, it ignores any
errors, but if the `PANDOC_FILTER_FAIL` environment variable is set
to 1, it fails if there were any errors. This scenario verifies that
this happens. The happy cases are already verified by other scenarios,
so this one only verifies handling of bad input.

~~~scenario
given an installed Rust program pandoc-filter-diagram
given file bad.md
when I run pandoc --filter pandoc-filter-diagram bad.md -o bad.html
then exit code is 0
when I try to run env PANDOC_FILTER_FAIL=1 pandoc --filter pandoc-filter-diagram bad.md -o bad.html
then command fails
~~~

~~~{#bad.md .file .markdown .numberLines}
trigraph "example" {
thing -> other
}
~~~

