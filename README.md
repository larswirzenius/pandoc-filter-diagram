# pandoc-filter-diagram -- render diagram markup in a Pandoc AST as SVG

[Pandoc]: https://pandoc.org/

NOTE: This doesn't seem to work with current versions of Pandoc (the
AST has changed in some way), and I no longer have any use for it
myself. If you'd like to take it over, please do. Fork the repository
and tell me, and I'll link to yours here.

[Pandoc][] is a powerful tool for processing documents in various
formats. It parses each of its supported input formats into an
in-memory abstract syntax tree, and then renders any supported output
format from such a tree. In between, it can run an arbitrary program
to "filter" the tree, and the program can make any arbitrary changes
to the tree.

This Rust crate provides a library to render as SVG diagrams in
various markup languages, embedded in Markdown as [fenced code blocks][]
that are marked with the appropriate class.

~~~~~~~~markdown
# Example

The following is a simplistic diagram using Graphviz dot language.

~~~dot
digraph "example" { thing -> other }
~~~
~~~~~~~~

This crate also provides a command line program,
`pandoc-filter-diagram`, which can be used with the Pandoc `--filter`
command line option.

## Languages

The library supports the following languages:

- [pikchr](https://pikchr.org/home/doc/trunk/homepage.md)
- [Graphviz dot](https://graphviz.org/doc/info/lang.html)
- [PlantUML](https://plantuml.com/)
- [roadmap](https://gitlab.com/larswirzenius/roadmap)
- [raw SVG](https://en.wikipedia.org/wiki/Scalable_Vector_Graphics)


# Licence

## The MIT License (MIT)

Copyright 2019-2021  Lars Wirzenius, Daniel Silverstone, pep.foundation

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
“Software”), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Fork this project to create your own MIT license that you can always
link to.
