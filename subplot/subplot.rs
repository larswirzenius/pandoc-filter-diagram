// Rust support for running Subplot scenarios for
// pandoc-filter-diagram.

use subplotlib::steplibrary::runcmd::Runcmd;

use std::env;
use std::path::Path;

#[step]
#[context(Runcmd)]
fn install_rust_program(context: &ScenarioContext, name: &str) {
    println!("install_rust_program 1");
    println!("install_rust_program: name={:?}", name);
    let target_dir = Path::new(env!("CARGO_TARGET_DIR"));
    let exe = target_dir.join("debug").join(name);
    println!("install_rust_program: exe={:?} {}", exe, exe.exists());
    assert!(exe.exists());
    println!("install_rust_program 2");
    let path = Path::new(&exe).parent().ok_or("No parent?")?;
    println!("install_rust_program 3");

    context.with_mut(
        |context: &mut Runcmd| {
            context.prepend_to_path(path);
            Ok(())
        },
        false,
    )?;
    println!("install_rust_program 999");
}
