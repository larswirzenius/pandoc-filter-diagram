use std::path::Path;

fn main() {
    subplot_build::codegen(Path::new("pandoc-filter-diagram.subplot"))
        .expect("failed to generate code with Subplot");
}
